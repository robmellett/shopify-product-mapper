# Quickly map data from files to the Shopify Product format

See this document for reference [Using CSV files](https://help.shopify.com/en/manual/products/import-export/using-csv)