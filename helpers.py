#!/usr/bin/env python
from slugify import slugify

def handle(value):
    return slugify(value)