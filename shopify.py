# /usr/bin/python
import requests
import secrets
from requests.auth import HTTPBasicAuth

class Shopify:

    def __init__(self):
        pass

    def get(self, uri, params = {}):
        json = requests.get(
             uri,
             headers={
                 "Content-Type": "application/json",
             },
             auth=HTTPBasicAuth(secrets.shopify['shopify_user'],
                                secrets.shopify['shopify_password']),
             params=params).json()
    
        return json

    def post(self, uri, params = {}):
        request = requests.post(
            uri,
            headers={
                "Content-Type": "application/json",
            },
            auth=HTTPBasicAuth(secrets.shopify['shopify_user'],
                               secrets.shopify['shopify_password']),
            json=params
        ).json()

        return request

    def delete(self, uri, params = {}):
        request = requests.delete(
            uri,
            headers={
                "Content-Type": "application/json",
            },
            auth=HTTPBasicAuth(secrets.shopify['shopify_user'],
                               secrets.shopify['shopify_password'])
        ).json()

        return request

    def getProducts(self, limit, page):
        print(">>> Fetching all products (%s)" % (page))

        shopifyUri = "{0}/{1}".format(
            secrets.shopify['shopify_uri'], 'admin/products.json')

        productsJson = self.get(shopifyUri)

        return productsJson["products"]

    def getProduct(self, shopifyProductId):
        print(">>> Fetching single product (%s)" % (shopifyProductId))

        shopifyUri = "{0}/{1}/{2}.json".format(
            secrets.shopify['shopify_uri'], 'admin/products', shopifyProductId)

        productJson = self.get(shopifyUri)

        return productJson

    def postProduct(self, params = {}):
        print(">>> Posting product to Shopify(%s)" % (params['product']['handle']))
        
        shopifyUri = "{0}/{1}.json".format(secrets.shopify['shopify_uri'], 'admin/products')

        productJson = self.post(shopifyUri, params)

        return productJson

    def uploadImageToProduct(self, shopifyProductId, variantId, filename, base64Attachment, position):
        shopifyUri = "{0}/{1}/{2}/images.json".format(
            secrets.shopify['shopify_uri'], 'admin/products', shopifyProductId)

        print(">>> Uploading Image to Product ('%s' at position '%s')" % (shopifyProductId, position))

        payload = {
            "image": {
                "variant_ids": [
                    variantId
                ],
                "attachment": base64Attachment,
                "filename": filename,
                "position": position
            }
        }

        productJson = self.post(shopifyUri, payload)

        return productJson

    def getProductImages(self, shopifyProductId):
        shopifyUri = "{0}/{1}/{2}/images.json".format(
            secrets.shopify['shopify_uri'], 'admin/products', shopifyProductId)

        request = requests.get(
            shopifyUri,
            headers={
                "Content-Type": "application/json",
            },
            auth=HTTPBasicAuth(secrets.shopify['shopify_user'],
                               secrets.shopify['shopify_password'])
        ).json()

        return request['images']

    def getProductCount(self):
        print(">>> Fetching product count")

        shopifyUri = "{0}/{1}".format(
            secrets.shopify['shopify_uri'], 'admin/products/count.json')

        productJson = self.get(shopifyUri)

        return productJson['count']

    def deleteProductImage(self, shopifyProductId, imageId):
        shopifyUri = "{0}/{1}/{2}/images/{3}.json".format(
            secrets.shopify['shopify_uri'], 'admin/products', shopifyProductId, imageId)

        request = self.delete(shopifyUri)

        return request
