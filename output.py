#! /usr/bin/python
# Handle Product data


class OutputHandler:

    def __init__(self, filename):
        self.data = []
        self.location = "./data/" + filename
        self.clearFile()

    def add(self, data):
        self.data.append(data)
        self.write()

    def write(self):
        file = open(self.location, "a", encoding="utf-8")
        line = ""

        for item in self.data:
            for value in item.values():
                line += str(value)

                # If not last item
                if value != list(item.values())[-1]:
                    line += ","
                else:
                    line += "\r"
        
        try:
            file.write(line)
            file.close()
        except Exception as excep:
            print(self.data)
            exit()

    def clearFile(self):
        open(self.location, 'w').close()
