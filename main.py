#!/usr/bin/env python
import json
from database import Database
from shopify import Shopify
from shopify_product import ShopifyProduct
from output import OutputHandler

# Enable Logging
# logging.basicConfig(level=logging.DEBUG, format=' %(asctime)s - %(levelname)s - %(message)s')

def getData():
    with open('./data/bestbuy_seo.json', 'r+', encoding="utf-8") as json_file:
        data = json.load(json_file)

        return data

def main():
    shopifyApi = Shopify()
    productData = getData()
    counter = 1

    # Iterate through unique products
    for product in list({p['name']: p for p in productData}.values()):
        
        print("Processing %s of %s" % (counter, len(productData)))
        
        shopify_product = ShopifyProduct(product)

        response = shopifyApi.postProduct(shopify_product.toShopifyApiProduct())

        counter = counter + 1

    print(">>> Upload Complete")

if __name__ == "__main__":
    main()
