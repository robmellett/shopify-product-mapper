# /usr/bin/python

database = {
    'host': "localhost",
    'user': "homestead",
    'passwd': "secret",
    'database': "python_test"
}

shopify = {
    'environment': 'staging',
    'shopify_uri': '',
    'shopify_user': '',
    'shopify_password': ''
}
