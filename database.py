import secrets
import mysql.connector as mysql

# Tutorial
# https: // www.datacamp.com/community/tutorials/mysql-python

class Database:

    def __init__(self):

        self.db = mysql.connect(
            host=secrets.database['host'],
            user=secrets.database['user'],
            passwd=secrets.database['passwd'],
            database=secrets.database['database']
        )

        # Creating an instance of 'cursor' class which is 
        # used to execute the 'SQL' statements in 'Python'
        self.cursor = self.db.cursor()

        
    def setUp(self):
        self.cursor.execute(
            "CREATE TABLE users (name VARCHAR(255), user_name VARCHAR(255))")
